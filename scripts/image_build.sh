#!/bin/bash
project=$(gcloud config get-value project)
docker pull gradle:5.4.1-jdk8 
docker pull openjdk:8-jdk-alpine 
docker pull postgres:11.3 
docker pull swaggerapi/swagger-ui:latest
docker build -t client_api:v1 ./client/
docker tag client_api:v1 gcr.io/$project/client_api:v1

docker build -t client_db:v1 ./database/
docker tag client_db:v1 gcr.io/$project/client_db:v1

docker tag swaggerapi/swagger-ui:latest client_swagger:v1
docker tag client_swagger:v1 gcr.io/$project/client_swagger:v1