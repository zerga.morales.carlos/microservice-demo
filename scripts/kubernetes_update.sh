#!/bin/bash
project=$(gcloud config get-value project)
echo $project
destiny=$(pwd)/kubernetes/
source=$destiny/template/

for source_file in $(ls $source) ; do
echo $source_file
sed -e "s/__PROJECT__ID__/$project/g" $source$source_file > $destiny$source_file
done
