#!/bin/bash
project=$(gcloud config get-value project)

docker push gcr.io/$project/client_api:v1

docker push gcr.io/$project/client_db:v1

docker push gcr.io/$project/client_swagger:v1