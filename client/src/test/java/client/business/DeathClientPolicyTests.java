package client.business;


import client.model.ClientEntity;
import junit.framework.TestCase;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class DeathClientPolicyTests extends TestCase {

    private DeatchClientPolicy deatchClientPolicy = new DeatchClientPolicy();

    public void testAddDays() throws Exception {
        ClientEntity clientEntity = new ClientEntity();

        clientEntity.setDateOfBirth(LocalDate.of(1994, 12, 12));
        assertEquals(this.deatchClientPolicy.addDays(clientEntity), 6518);

        clientEntity.setDateOfBirth(LocalDate.of(2012, 01, 01));
        assertEquals(this.deatchClientPolicy.addDays(clientEntity), 2389);

        clientEntity.setDateOfBirth(LocalDate.of(1994, 04, 14));
        assertEquals(this.deatchClientPolicy.addDays(clientEntity), 7152);
    }

    public void testAddMonths() throws Exception {
        ClientEntity clientEntity = new ClientEntity();

        clientEntity.setFirstName("Carlos");
        clientEntity.setLastName("Zerga");
        assertEquals(this.deatchClientPolicy.addMonths(clientEntity), 11);

        clientEntity.setFirstName("Juan Gabriel");
        clientEntity.setLastName("Miro Quesada");
        assertEquals(this.deatchClientPolicy.addMonths(clientEntity), 24);

        clientEntity.setFirstName("");
        clientEntity.setLastName("Miro Quesada");
        assertEquals(this.deatchClientPolicy.addMonths(clientEntity), 12);
    }

    public void testAddYears() throws Exception {
        ClientEntity clientEntity = new ClientEntity();

        clientEntity.setDateOfBirth(LocalDate.of(1994, 12, 12));
        assertEquals(this.deatchClientPolicy.addYears(clientEntity), 10);


        clientEntity.setDateOfBirth(LocalDate.of(1993, 10, 05));
        assertEquals(this.deatchClientPolicy.addYears(clientEntity), 26);

        clientEntity.setDateOfBirth(LocalDate.of(1972, 07, 15));
        assertEquals(this.deatchClientPolicy.addYears(clientEntity), 1);
    }


    public void testDeathDate() throws Exception {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setLastName("Uchurra");
        clientEntity.setFirstName("Pedro");
        clientEntity.setDateOfBirth(LocalDate.of(1994, 12, 12));
        assertEquals(this.deatchClientPolicy.estimate(clientEntity).format(dateTimeFormatter), "2023-10-16");

        clientEntity.setLastName("Quispe");
        clientEntity.setFirstName("Dario");
        clientEntity.setDateOfBirth(LocalDate.of(1930, 10, 1));
        assertEquals(this.deatchClientPolicy.estimate(clientEntity).format(dateTimeFormatter), "1960-04-12");

        clientEntity.setLastName("Juana Arco");
        clientEntity.setFirstName("La Mora");
        clientEntity.setDateOfBirth(LocalDate.of(2010, 6, 4));
        assertEquals(this.deatchClientPolicy.estimate(clientEntity).format(dateTimeFormatter), "2036-07-14");

    }
}
