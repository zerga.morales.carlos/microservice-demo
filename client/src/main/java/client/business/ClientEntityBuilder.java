package client.business;

import client.application.request.Client;
import client.model.ClientEntity;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ClientEntityBuilder {

    public static ClientEntity build(Client client) {
        return new ClientEntity(
                client.getLastName(),
                client.getFirstName(),
                client.getAge(),
                LocalDate.parse(client.getDateOfBirth(), DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        );
    }
}
