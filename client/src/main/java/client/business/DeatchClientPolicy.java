package client.business;

import client.application.request.Client;
import client.model.ClientEntity;

import java.time.LocalDate;

/**
 * LOCAPI FORMULA
 * It's a formula that predict your death date based on: firstName, lastName, birth. Indeed adds
 * days/weeks/months/years based on parameters before mentioned to your birth date
 * <p>
 * FORMULA:
 * - Days added: obtain day of era but reversing unit order
 * - Months added: FirstName Length + LastName Length
 * - Years added: Modulus from Day of Year based on 28
 */
public class DeatchClientPolicy {

    public LocalDate estimate(ClientEntity clientEntity) {
        int moreDays = this.addDays(clientEntity);
        int moreMonths = this.addMonths(clientEntity);
        int moreYears = this.addYears(clientEntity);
        return clientEntity.getDateOfBirth()
                .plusDays(moreDays)
                .plusMonths(moreMonths)
                .plusYears(moreYears);
    }

    public int addDays(ClientEntity clientEntity) {
        return clientEntity.getDateOfBirth().getYear() +
                clientEntity.getDateOfBirth().getMonth().getValue() * 12 +
                clientEntity.getDateOfBirth().getDayOfMonth() * 365;
    }

    public int addMonths(ClientEntity clientEntity) {
        return clientEntity.getLastName().length() + clientEntity.getFirstName().length();
    }

    public int addYears(ClientEntity clientEntity) {
        return clientEntity.getDateOfBirth().getDayOfYear() % 28;
    }
}
