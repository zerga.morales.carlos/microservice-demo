package client.service;

import client.model.ClientEntity;
import client.model.KpiEntity;
import java.util.List;

public interface ClientService {

    ClientEntity save(ClientEntity client);

    KpiEntity stats();

    List<ClientEntity> list();
}
