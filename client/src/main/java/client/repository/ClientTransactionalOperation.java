package client.repository;

import client.model.ClientEntity;

import java.util.List;

public interface ClientTransactionalOperation {

    public ClientEntity save(ClientEntity clientEntity);

    public List<ClientEntity> findAll();

    public double calculateMeanAge();

    public double calculateStandardDesviation();
}
