package client.application;

import client.application.request.Client;
import client.business.ClientEntityBuilder;
import client.business.DeatchClientPolicy;
import client.model.ClientEntity;
import client.model.KpiEntity;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClientServiceController {

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/creacliente", method = RequestMethod.POST)
    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Create a new client")
    public ResponseEntity<ClientEntity> createClient(@Validated(Client.OrderedChecks.class) @RequestBody Client client) {
        return ResponseEntity.ok(clientService.save(ClientEntityBuilder.build(client)));
    }

    @ApiOperation(value = "Show statistics about client repository")
    @RequestMapping(value = "/kpiclientes", method = RequestMethod.GET)
    public ResponseEntity<KpiEntity> kpi() {
        return ResponseEntity.ok(clientService.stats());
    }

    @ApiOperation(value = "List all clients with it's death date")
    @RequestMapping(value = "/listclientes", method = RequestMethod.GET)
    public ResponseEntity<List<ClientEntity>> listClients() {
        List<ClientEntity> clientList = clientService.list();
        clientList.stream().forEach((client)->client.makeDateOfDeath(new DeatchClientPolicy()));
        return ResponseEntity.ok(clientList);
    }
}
