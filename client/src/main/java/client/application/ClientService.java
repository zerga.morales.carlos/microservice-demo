package client.application;


import client.infraestructure.query.ClientRepository;
import client.model.AttributeStatistics;
import client.model.ClientEntity;
import client.model.KpiEntity;
import client.repository.ClientTransactionalOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService implements client.service.ClientService {

    @Autowired
    private ClientTransactionalOperation clientTransactionalOperation;

    @Override
    public ClientEntity save(ClientEntity client) {
        return clientTransactionalOperation.save(client);
    }

    @Override
    public KpiEntity stats() {
        KpiEntity kpiEntity = new KpiEntity();
        AttributeStatistics age = new AttributeStatistics(
                clientTransactionalOperation.calculateMeanAge(),
                clientTransactionalOperation.calculateStandardDesviation()
        );
        kpiEntity.setAge(age);
        return kpiEntity;
    }

    @Override
    public List<ClientEntity> list() {
        return clientTransactionalOperation.findAll();
    }
}
