package client.application.request;

import client.application.validator.BirthDateAge;
import client.application.validator.DateFormat;
import client.application.validator.WithoutDigits;
import client.model.ClientEntity;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.GroupSequence;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


@BirthDateAge(
        birthField = "dateOfBirth",
        ageField = "age",
        message = "Client age and date of birth should match with same client years old",
        dateFormat = "yyyy-MM-dd",
        groups = Client.Fourth.class
)
public class Client {

    @NotNull(message = "Client last name is required", groups = First.class)
    @WithoutDigits(message = "Client last name can't contain digits", groups = Second.class)
    @NotEmpty(message = "Client last name can't be empty", groups = Third.class)
    @Size(min = 3, max = 50, message = "Client last name length should be between 3 and 50 characters", groups = Fourth.class)
    @ApiModelProperty(notes = "Last Name client", dataType = "string", example = "Carlos", required = true, position = 0)
    private String lastName;

    @NotNull(message = "Client first name is required", groups = First.class)
    @WithoutDigits(message = "Client last name can't contain digits", groups = Second.class)
    @NotEmpty(message = "Client first name can't be empty", groups = Third.class)
    @Size(min = 3, max = 50, message = "Client first name length should be between 3 and 50 characters", groups = Fourth.class)
    @ApiModelProperty(notes = "First Name client", dataType = "string", example = "Carlos", required = true, position = 1)
    private String firstName;


    @NotNull(message = "Client date of birth is required", groups = First.class)
    @NotEmpty(message = "Client date of birth can't be empty", groups = Second.class)
    @DateFormat(message = "Client date of birth should have yyyy-MM-dd date format", format = "yyyy-MM-dd", groups = Third.class)
    @ApiModelProperty(notes = "Date of Birth client", dataType = "string", example = "1994-04-14", required = true, position = 2)
    private String dateOfBirth;

    @NotNull(message = "Client age is required", groups = First.class)
    @Min(value = 1, message = "Client age should be grather than 0", groups = Second.class)
    @ApiModelProperty(notes = "Age Client, should be equivalent to date of birth", dataType = "int", example = "25", required = true, position = 3)
    private Integer age;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getAge() {
        return age;
    }


    @GroupSequence(value = {Default.class, First.class, Second.class, Third.class, Fourth.class})
    public interface OrderedChecks {
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }

    interface Fourth {
    }
}
