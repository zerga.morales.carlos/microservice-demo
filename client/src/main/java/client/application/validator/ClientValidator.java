package client.application.validator;

import client.application.request.Client;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ClientValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Client.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        Client c = (Client) target;

        errors.reject("all", "First Validation");
    }


}
