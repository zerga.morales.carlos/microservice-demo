package client.application.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class WithoutDigitsValidator implements
        ConstraintValidator<WithoutDigits, String> {

    @Override
    public void initialize(WithoutDigits constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        return !value.chars().allMatch(Character::isDigit);
    }
}
