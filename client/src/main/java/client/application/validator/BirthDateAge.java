package client.application.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BirthDateAgeValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface BirthDateAge {
    String message() default "Fields values don't match!";

    Class<?>[] groups() default {};

    String birthField();

    String ageField();

    String dateFormat() default "yyyy-MM-dd";

    Class<? extends Payload>[] payload() default {};
}
