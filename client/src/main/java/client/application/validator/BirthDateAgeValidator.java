package client.application.validator;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class BirthDateAgeValidator
        implements ConstraintValidator<BirthDateAge, Object> {

    private String ageField;

    private String birthField;

    private String dateFormat;

    @Override
    public void initialize(BirthDateAge constraintAnnotation) {
        this.ageField = constraintAnnotation.ageField();
        this.birthField = constraintAnnotation.birthField();
        this.dateFormat = constraintAnnotation.dateFormat();

    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        try {
            int ageValue = Integer.parseInt(Objects.requireNonNull(new BeanWrapperImpl(value)
                    .getPropertyValue(this.ageField)).toString());
            String birthValue = Objects.requireNonNull(new BeanWrapperImpl(value)
                    .getPropertyValue(this.birthField)).toString();
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(this.dateFormat);

            LocalDate birthDate = LocalDate.parse(birthValue, dateTimeFormatter);

            LocalDate currentDay = LocalDate.now();

            return Period.between(birthDate, currentDay).getYears() == ageValue;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }
}
