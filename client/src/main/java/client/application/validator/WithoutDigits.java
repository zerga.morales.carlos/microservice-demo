package client.application.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = WithoutDigitsValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)

public @interface WithoutDigits {
    String message() default "Field can't contain digits";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
