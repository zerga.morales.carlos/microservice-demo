package client.infraestructure.query;

import client.model.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Integer> {

    @Query(value = "select avg(age) from clients")
    double calculateMeanAge();

    @Query(value = "select stddev(age) from clients")
    double calculateStandardDesviationAge();
}
