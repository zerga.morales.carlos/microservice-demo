package client.infraestructure;

import client.infraestructure.query.ClientRepository;
import client.model.ClientEntity;
import client.repository.ClientTransactionalOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientDatabaseRepository implements ClientTransactionalOperation {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public ClientEntity save(ClientEntity clientEntity) {
        return clientRepository.save(clientEntity);
    }

    @Override
    public List<ClientEntity> findAll() {
        return clientRepository.findAll();
    }

    @Override
    public double calculateMeanAge() {
        return clientRepository.calculateMeanAge();
    }

    @Override
    public double calculateStandardDesviation() {
        return clientRepository.calculateStandardDesviationAge();
    }
}
