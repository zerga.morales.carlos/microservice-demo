package client.model;

import client.business.DeatchClientPolicy;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "clients")
@ApiModel(description = "Class representing a client tracked by the application.")
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "Unique identifier of the client. No two clients can have the same id.", example = "5", required = false, accessMode = ApiModelProperty.AccessMode.READ_ONLY, position = 0)
    private Integer id;

    @ApiModelProperty(notes = "Last Name client", dataType = "string", example = "Zerga", required = true, position = 1)
    private String lastName;

    @ApiModelProperty(notes = "First Name client", dataType = "string", example = "Carlos", required = true, position = 2)
    private String firstName;

    @ApiModelProperty(notes = "Age Client", dataType = "integer", example = "25", required = true, position = 3)
    private int age;

    @ApiModelProperty(notes = "Date of Birth Client", dataType = "date", example = "1994-04-14", required = true, position = 4)
    private LocalDate dateOfBirth;

    @Transient
    @ApiModelProperty(notes = "Date of Death Client", dataType = "date", example = "2014-12-21", required = false,accessMode = ApiModelProperty.AccessMode.READ_ONLY, position = 5)
    private LocalDate dateOfDeath;

    public ClientEntity() {
    }

    public ClientEntity(String lastName, String firstName, int age, LocalDate dateOfBirth) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public LocalDate getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(LocalDate dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public void makeDateOfDeath(DeatchClientPolicy deathPolicy) {
        this.dateOfDeath = deathPolicy.estimate(this);
    }
}
