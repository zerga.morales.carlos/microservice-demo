package client.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Entity that represents statistics from Client.")
public class KpiEntity {

    @ApiModelProperty(notes = "Statistics related to age", dataType = "client.model.AttributeStatistics", required = true, position = 0)
    private AttributeStatistics age;

    public AttributeStatistics getAge() {
        return age;
    }

    public void setAge(AttributeStatistics age) {
        this.age = age;
    }
}

