package client.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Entity that represents any kind field statistics.")
public class AttributeStatistics {
    @ApiModelProperty(notes = "Mean Statistics from Field", dataType = "double", example = "23.45", required = true, position = 0)
    private double mean;
    @ApiModelProperty(notes = "Standard Deviation from Field", dataType = "double", example = "0.345", required = true, position = 0)
    private double standardDesviation;

    public AttributeStatistics(double mean, double standardDesviation) {
        this.mean = mean;
        this.standardDesviation = standardDesviation;
    }

    public double getMean() {
        return mean;
    }

    public void setMean(double mean) {
        this.mean = mean;
    }

    public double getStandardDesviation() {
        return standardDesviation;
    }

    public void setStandardDesviation(double standardDesviation) {
        this.standardDesviation = standardDesviation;
    }
}