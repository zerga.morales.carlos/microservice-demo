# Micro-services project demo

## Project Structure
Brief explanation about each folder contained in this repository
* **scripts** all bash scripts related to specific tasks: like building and uploading images and updating yml definitions with current user
* **client** all java code for client microservice application
* **database** all configuration for database related to client miroservice 
* **kubernetes** all configuration files related to application upload into a kubernetes cluster

## Services Exposed

* **client** This service is related to client api 
* **database** This is a database service for client api
* **swagger** This is a swagger entrypoint for client api

## Installing Dependencies

1. [gcloud](https://cloud.google.com/sdk/install): for  *Google Cloud Platafform* administration from your command line
>Version:  Google Cloud SDK 247.0.0
2. [docker](https://docs.docker.com/install/): for building images and running containers in your local machine
>Version:  18.09.6, build 481bc77
3. [docker-compose](https://docs.docker.com/compose/install/): for creating and environment in your local machine to allows you to orchestate all containers project
>Version:  1.23.1, build b02f1306
4. [kubectl](https://kubernetes.io/es/docs/tasks/tools/install-kubectl/): for managing kubernetes cluster, plataform independent
>Version:  v1.14.0


## Run in docker-compose:
1.- Build image
```
$ ./scripts/image_build.sh
```
2.- Run docker compose
```
$ docker-compose up -d
```
3.- Watch ports

[Api CLient](http://localhost:8080)

[Swagger CLient](http://localhost:81)

## Configuration for Kubernetes:
1. Create/Configure [*GCP*](https://console.cloud.google.com/home/dashboard) account
2. Create a [New Project](https://console.cloud.google.com/projectcreate)
3. Enable following *GCP* services
>All this API services should be enabled with an specific *Project ID*

* [Compute Engine API](https://console.cloud.google.com/apis/api/compute.googleapis.com/overview)
* [Kubernetes Engine API](https://console.cloud.google.com/apis/api/container.googleapis.com/overview)
* [Container Registry API](https://console.cloud.google.com/apis/api/containerregistry.googleapis.com/overview)

4. Initialize *gcloud* sdk with following commands

```
$ gcloud auth login 
$ gcloud auth configure-docker
$ gcloud config set project $(gcloud projects list --uri --limit 1 | awk 'BEGIN{FS="/"}{print $6}')
# In case you have specific project ID provide it in previous command instead of a default one
$ gcloud config set compute/zone us-central1-b
# In case you have specific compute zone provide it
```
5. Create Cluster in GCP 
```
$ gcloud container clusters create microservices --num-nodes=1
```
6. Configure docker with kubernetes in GCP
```
$ gcloud auth configure-docker
$ docker login -u oauth2accesstoken -p "$(gcloud auth print-access-token)" https://gcr.io
```
7. Build docker images and upload it by running scripts
```
$ ./scripts/image_build.sh
$ ./scripts/image_upload.sh
```
8. Create yml with your Project ID and Create it in GCP
```
$ ./scripts/kubernetes_update.sh
$ kubectl apply -k kubernetes/
```

9. Wait for a few minutes until GCP allows you with a public IP
```
$ kubectl get services 
```
![alt text](image.png "Logo Title Text 1")

When External-api for *client-api* has an IP that means that is now available.

>Currently swagger service in kubernetes is not working due to CORS, it works with fixed IP, you should put client-api *External-IP* to make it work

![alt text](swagger.png "Logo Title Text 1")